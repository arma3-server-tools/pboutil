import pytest
import pboutil


@pytest.fixture(scope="session")
def pbo_file(tmpdir_factory):
    p = pboutil.PBOFile()
    p.add_file_data('testfile', b'test data')
    p.add_file_data('b', b'b content')
    p.add_file_data('a', b'a content')
    p.add_file_data('c', b'c content')
    fn = tmpdir_factory.mktemp("data").join("test.pbo")
    p.to_file(fn)
    return fn


def test_bytes(pbo_file):
    p = pboutil.PBOFile.from_file(pbo_file)
    b = p.as_bytes()
    p2 = pboutil.PBOFile.from_bytes(b)

    assert p.as_bytes() == p2.as_bytes()
    assert list(p.filenames()) == list(p2.filenames())
    assert p.file_as_bytes('testfile') == p2.file_as_bytes('testfile')


def test_remove_file(pbo_file):
    p = pboutil.PBOFile.from_file(pbo_file)
    b = p.as_bytes()
    p.remove_file("testfile")
    b2 = p.as_bytes()

    assert b != b2


def test_filenames_order(pbo_file):
    p = pboutil.PBOFile.from_file(pbo_file)
    filenames = list(p.filenames())

    assert filenames == sorted(filenames)


def test_files_invalid_name(pbo_file):
    p = pboutil.PBOFile.from_file(pbo_file)
    with pytest.raises(RuntimeError):
        p.file_as_bytes('notfound')


def test_files_add_duplicate(pbo_file):
    p = pboutil.PBOFile.from_file(pbo_file)
    with pytest.raises(RuntimeError):
        p.add_file_data('double', 'once')
        p.add_file_data('double', 'twice')


def test_directory(pbo_file, tmpdir):
    dirname = tmpdir.mkdir("pbodir")
    p = pboutil.PBOFile.from_file(pbo_file)

    p.to_directory(dirname)
    p2 = pboutil.PBOFile.from_directory(dirname)

    assert p.as_bytes() == p2.as_bytes()
    assert list(p.filenames()) == list(p2.filenames())
    assert p.file_as_bytes('testfile') == p2.file_as_bytes('testfile')
    assert len(dirname.listdir()) == 4


def test_extended(pbo_file):
    import arrow
    import construct

    arrow_z = arrow.Arrow(1970, 1, 1, 0, 0, 0)

    p = pboutil.PBOFile.from_file(pbo_file)
    p._pbo.header.insert(0, construct.Container(filename=u'', method=0, originalsize=0, timestamp=arrow_z, datasize=0, meta=True, final=False, extended=True, extension=['foo', 'bar', '']))
    b = p.as_bytes()
    p2 = pboutil.PBOFile.from_bytes(b)

    assert p.as_bytes() == p2.as_bytes()
    assert list(p.filenames()) == list(p2.filenames())
    assert p.file_as_bytes('testfile') == p2.file_as_bytes('testfile')

    p.remove_file("testfile")
    b2 = p.as_bytes()
    assert b != b2

    p.add_file_data('testfile', b'test data')
    b3 = p.as_bytes()
    assert b == b3
