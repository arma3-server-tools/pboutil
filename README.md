# pboutil

ArmA PBO editing utility.

Currently maintained here:

- Source repository: https://gitlab.com/arma3-server-tools/pboutil
- Python module in PyPI: https://pypi.org/project/pboutil/

Automatically generated artifacts:
- Module documentation: https://arma3-server-tools.gitlab.io/pboutil/docs/pboutil.html
- Coverage results: https://arma3-server-tools.gitlab.io/pboutil/coverage/index.html

## Resources

### PBO File format

- https://resources.bisimulations.com/wiki/PBO_File_Format

### arma_pbo.ksy

- [arma_pbo.ksy](arma_pbo.ksy) is maintained in this repository
- Kaitai parser based on the ArmA PBO File Format
