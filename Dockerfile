FROM python:3
RUN pip install --upgrade pip

WORKDIR /usr/src/app

COPY requirements.txt README.md setup.py tox.ini ./
COPY pboutil/ pboutil/
RUN pip install . && \
    pip install --no-cache-dir -r requirements.txt

COPY runpatches.py .
COPY tests/ tests/
#COPY example.pbo example.zip ./

# this behavior will change
CMD [ "python", "-i" ]

# or to run patcher
#CMD [ "python", "runpatches.py" ]

# docker run -it --rm pboutil
# docker run -it --rm -v `pwd -P`/mpmissions/:/mpmissions/ -v `pwd -P`/patches/:/patches/ pboutil python runpatches.py
